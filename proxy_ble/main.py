from paho.mqtt import client as mqtt
import json
import time
from ScanDelegate import ScanDelegate
from bluepy.btle import Scanner, Peripheral

# reading the config file
config = {}
with open('config.txt', 'r') as f:
    config = json.loads(f.read())

# on mqtt connect
def on_connect(client, userdata, flag, rc):
    print('connected to : ' + userdata['broker']['host'])
    t = userdata['topics']['downlink']
    print('subscribe to: ' + t)
    client.subscribe(t)

# handler user downlink message
def on_downlink_message(obj, userdata, msg):
    print(msg.payload)
    # Helper.sendBleDownlink(config, message)
    #connecting to device
    mac = msg.topic[4:21]
    print('connecting to ' + mac + '...')
    try:
        with Peripheral(mac, 'random') as p:
            print('connected')
            w = p.getCharacteristics(uuid=userdata['ble']['nordic_uart']['tx_uuid'])[0]
            data = json.loads(msg.payload)
            for sen in data['sensors']:
                print('writing message to device: ')
                value = hex(sen['id']) + base64.b64decode(sen['value']).encode('hex')
                w.write(value)
                time.sleep(0.5)
    except Exception as e:
        print(e) 


# connecting to mqtt broker
client = mqtt.Client(userdata = config)
client.username_pw_set(username=config['broker']['username'], password=config['broker']['password'])
client.on_connect = on_connect
client.on_message = on_downlink_message
client.connect(host=config['broker']['host'])
client.loop_start()

# ble scanner with ScanDelegate class
scanner = Scanner().withDelegate(ScanDelegate(config, client))

    
# start the forever loop
while True:
    scanner.scan(config['ble']['scan_timeout'])