from bluepy.btle import DefaultDelegate
import base64
import json

class ScanDelegate(DefaultDelegate):
    def __init__(self, config, mqttc):
        self.mqttc = mqttc
        #self.manufacturer_id = config['ble']['manufacturer_id']
        self.config = config
        DefaultDelegate.__init__(self)
    
    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewData:
            result = {}
            result['mac'] = dev.addr
            sensors = []
            counter = 0
            for (adtype, desc, value) in dev.getScanData():
                if desc == 'Manufacturer' and value.startswith(self.config['ble']['manufacturer_id']):
                    counter = counter + 1
                    sen = {}
                    sen['id'] = value[4:6]
                    sen['value'] = base64.b64encode(value[6:])
                    sensors.append(sen)
                    print(sen)
            result['sensor'] = sensors
            if counter > 0:
                self.mqttcpublish_to_broker(self.mqttc, dev.addr, result)
                    
    def publish_to_broker(self, client, addr, value):
        # message = {"payload": base64.b64encode(value), "device_mac": addr}
        topic = self.config['topics']['uplink'].replace('<mac>', addr)
        client.publish(topic=topic, payload= json.dumps(value))